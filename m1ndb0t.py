import json
import sc2
from sc2.constants import *
import random
from sc2.client import *
from sc2.position import Point2
import os


def clearscreen():
    if os.name == 'nt':
        _ = os.system('cls')
    else:
        _ = os.system('clear')


def get_unitlist(enemyrace):
    """Reads Unit Build List"""
    data = {
        'Race.Terran': 'builds/unitlist_pvt_general.json',
        'Race.Protoss': 'builds/unitlist_pvp_general.json',
        'Race.Zerg': 'builds/unitlist_pvz_general.json',
        'Race.Random': 'builds/unitlist_pvr_general.json'
    }

    with open(data.get(enemyrace)) as src:
        return json.load(src)


def get_buildorder(enemyrace):
    """Reads BO"""
    data = {
        'Race.Terran': 'builds/buildorder_pvt_general.json',
        'Race.Protoss': 'builds/buildorder_pvp_general.json',
        'Race.Zerg': 'builds/buildorder_pvz_general.json',
        'Race.Random': 'builds/buildorder_pvr_general.json'
    }

    with open(data.get(enemyrace)) as src:
        return json.load(src)


class m1ndb0t(sc2.BotAI):
    def __init__(self):
        super().__init__()
        self.strategy = 'Default'
        self.warpdrop = False
        self.startpoint = False
        self.warpgate_started = False
        self.blink_started = False
        self.blink_done = False
        self.exlance_started = False
        self.shield_built = False
        self.nexusInfo = {}
        self.unitInfo = {}
        self.fightstarted = False
        self.enemyrace = "Unknown"
        self.enemy_id = 3
        self.attacked = False
        self.microActions = []
        self.targetunits = []
        self.coreboosted = False
        self.ramp_built = False
        self.ramp_location = False
        self.cheese_towerrush = False
        self.cheese_voidrays = False
        self.reactive_lifting = False
        self.commander = False
        self.commanderunit = None
        self.commanderposition = False
        self.commandertarget = None
        self.scouting = False
        self.proxy_built = False
        self.pylonthreshold = 7
        self.attack_count = 12
        self.retreat_count = 5
        self.my_armypower = 0
        self.enemy_armypower = 0
        self.my_armypower_older = 0
        self.enemy_armypower_older = 0
        self.my_status_older = 'Unknown'
        self.enemy_status_older = 'Unknown'
        self.my_status = 'Unknown'
        self.enemy_status = 'Unknown'
        self.startcounter = False
        self.attacklvl1_started = False
        self.attacklvl2_started = False
        self.attacklvl3_started = False
        self.armorlvl1_started = False
        self.armorlvl2_started = False
        self.armorlvl3_started = False
        self.shieldlvl1_started = False
        self.shieldlvl2_started = False
        self.shieldlvl3_started = False
        self.move2ramp = False
        self.maxhqcount = 4
        self.maxworkers = 0
        self.scouts = []
        self.pylonworkers = []
        self.waitlocation = False
        self.probeguard = False
        self.waitlocation = False
        self.attacktime = False
        self.attackamount = False
        self.knownattacktime = False
        self.reacted = False
        self.enemyrush = False
        self.enemyrush_cannons_done = False
        self.probescout = False
        self.neededworkers = 0
        self.enemycannons = False
        self.neednexus = False
        self.mapcorner_nw = False
        self.mapcorner_ne = False
        self.mapcorner_sw = False
        self.mapcorner_se = False
        self.map_center_n = False
        self.map_center_s = False
        self.map_center_e = False
        self.map_center_w = False
        self.map_center = False

        # Define tactics
        self.tactic_reactive_lifting = False
        self.tactic_warpprism = False
        self.tactic_darktemplar = False
        self.tactic_buildproxy = False

        # Enable Debug/Status
        self.debugmsg = False
        self.statusmsg = False

    async def timecheck(self):
        """Checks when 1st attack is made and writes to datafile"""
        if not self.attacktime:
            enemies = self.known_enemy_units.not_structure.closer_than(20, self.main_base_ramp.top_center.position).filter(lambda w: w.type_id not in [UnitTypeId.PROBE])
            enemyproxy = self.known_enemy_units.structure.closer_than(30, self.main_base_ramp.top_center.position)
            if enemies.amount >= 5 or enemyproxy:
                self.attacktime = self.time

                if self.debugmsg:
                    print("Got attacked at " + str(self.attacktime))
                await self.savedata()

    async def savedata(self):
        datafile = "./data/" + self.opponent_id + ".db"
        file = open(datafile, "w")
        file.write(str(self.attacktime))
        file.close()

    async def readdata(self):
        file = open("./data/" + self.opponent_id + ".db", "r")
        self.knownattacktime = file.readline()
        file.close()
        await self.reactontime()

    async def reactontime(self):
        if not self.reacted:
            if self.knownattacktime:
                if float(self.knownattacktime) < 300:
                    self.enemyrush = True
                    await self.chat_send("Do you also smell cheese?")
                    self.reacted = True

    async def reactoncannons(self):
        if self.enemycannons:
            return
        for enemy in self.known_enemy_units:
            if enemy.type_id == UnitTypeId.PHOTONCANNON:
                self.enemycannons = True

    async def reactonworkerrush(self):
        if not self.units(UnitTypeId.NEXUS).ready:
            return

        if self.units(UnitTypeId.PHOTONCANNON).ready:
            return

        fighterunits = self.units.not_structure.filter(lambda w: w.type_id not in [UnitTypeId.PROBE])
        if fighterunits.amount >= 3:
            return

        enemyprobes = self.known_enemy_units.not_structure.closer_than(20, self.main_base_ramp.top_center.position).filter(lambda w: w.type_id in [UnitTypeId.PROBE])
        if enemyprobes.amount >= 3:
            if not self.reacted:
                await self.chat_send("Workers? Boring...")
                self.reacted = True

            counterprobes = self.units(UnitTypeId.PROBE).ready
            for probe in counterprobes:
                distance2ramp = probe.position.distance_to(self.ramp_location)
                if distance2ramp < 30:
                    next_enemy = enemyprobes.closest_to(probe)
                    self.microActions.append(probe.attack(next_enemy))

    async def racecheck(self):
        """finds out enemy race and sets race specific values"""
        if self.player_id == 1:
            self.enemy_id = 2
        else:
            self.enemy_id = 1
        self.enemyrace = Race(self._game_info.player_races[self.enemy_id])

        # Should we counter with lift
        self.tactic_reactive_lifting = False

        # Should we build a proxy
        self.tactic_buildproxy = False

        # Should we build DTs
        self.tactic_darktemplar = False

        if self.enemyrace == Race.Terran:
            self.attack_count = 12
            self.retreat_count = 3
        elif self.enemyrace == Race.Protoss:
            self.attack_count = 12
            self.retreat_count = 3
        elif self.enemyrace == Race.Zerg:
            self.tactic_darktemplar = True
            self.tactic_warpprism = True
            self.attack_count = 12
            self.retreat_count = 3
        elif (self.enemyrace == Race.Random) or (self.enemyrace == "Unknown"):
            self.attack_count = 12
            self.retreat_count = 3
            self.enemyrush = True # Prepare for rush if enemy is unknown

    async def buildatramp(self):
        """Builds a Pylon at Ramp Position"""
        ramppylon = self.units.structure.filter(lambda w: w.type_id == UnitTypeId.PYLON).closer_than(5, self.main_base_ramp.top_center.position)
        if ramppylon:
            self.ramp_built = True
            return

        if self.can_afford(UnitTypeId.PYLON) and not self.ramp_built:
            pos = self.main_base_ramp.top_center.position
            nx = self.units(UnitTypeId.NEXUS).ready.first.position
            p = pos.towards(nx, 4)
            err = await self.build(UnitTypeId.PYLON, near=p)
            if not err:
                self.ramp_location = p
                return

    async def waitforit(self):
        """Sends a drone to next expansion location and waits for proxies"""
        if not self.probeguard:
            if not self.units(UnitTypeId.PROBE):
                return
            probe = self.units(UnitTypeId.PROBE).ready.first
            self.probeguard = probe.tag
        else:
            if not self.waitlocation:
                self.waitlocation = await self.get_next_expansion()
            guard = self.units.find_by_tag(self.probeguard)
            if not guard:
                self.probeguard = None
            else:
                distance2waitpos = guard.position.distance_to(self.waitlocation)
                if distance2waitpos >= 1:
                    self.microActions.append(guard.move(self.waitlocation))

    async def scoutwithworker(self):
        """Uses a Worker to scout the enemy"""
        if self.scouting:
            return

        if not self.probescout:
            if not self.units(UnitTypeId.PROBE).ready:
                return
            probes = self.units(UnitTypeId.PROBE).ready
            for probe in probes:
                if probe.is_idle and probe.tag != self.probeguard:
                    if self.debugmsg:
                        print("Scout: " + str(probe.tag))
                    self.probescout = probe.tag
                    return
        else:
            scout = self.units.find_by_tag(self.probescout)
            if not scout:
                self.probescout = False
                return
            else:
                for enemyStartLoc in list(self.enemy_start_locations):
                    self.microActions.append(scout.move(enemyStartLoc.position, queue=True))
                self.scouting = True
                return

    async def buildproxy(self):
        """Builds a Pylon somewhere around the opponent base"""
        if self.units(UnitTypeId.CYBERNETICSCORE).amount >= 1 and not self.proxy_built and self.can_afford(
                UnitTypeId.PYLON):
            if self.known_enemy_structures:
                target = self.known_enemy_structures.first.position
                nx = self.units(UnitTypeId.NEXUS).ready.first.position
                p = target.towards(nx.position, random.randrange(40, 50))

                proxypylon = self.units.structure.filter(lambda w: w.type_id == UnitTypeId.PYLON).closer_than(10, p)
                if proxypylon:
                    self.proxy_built = True
                    return

                await self.build(UnitTypeId.PYLON, near=p)
                self.proxy_built = True
                return

    async def scoutroutine(self, scout, corners):
        """Scouts for the enemy, needs a unit as param"""
        if not scout:
            return

        if scout.noqueue:
            for enemyStartLoc in list(self.enemy_start_locations):
                if self.debugmsg:
                    print("Sending Scout")

                if not corners:
                    if scout.noqueue:
                        self.microActions.append(scout.move(enemyStartLoc.position, queue=True))
                        self.microActions.append(scout.move(self.state.mineral_field.random.position, queue=True))

                else:
                    if scout.noqueue:
                        if self.startpoint == 'SW':
                            self.microActions.append(scout.move(self.mapcorner_se, queue=True))
                            self.microActions.append(scout.move(self.mapcorner_ne, queue=True))
                            self.microActions.append(scout.move(enemyStartLoc.position, queue=True))
                            self.microActions.append(scout.move(self.state.mineral_field.random.position, queue=True))
                        if self.startpoint == 'SE':
                            self.microActions.append(scout.move(self.mapcorner_sw, queue=True))
                            self.microActions.append(scout.move(self.mapcorner_nw, queue=True))
                            self.microActions.append(scout.move(enemyStartLoc.position, queue=True))
                            self.microActions.append(scout.move(self.state.mineral_field.random.position, queue=True))
                        if self.startpoint == 'NW':
                            self.microActions.append(scout.move(self.mapcorner_ne, queue=True))
                            self.microActions.append(scout.move(self.mapcorner_se, queue=True))
                            self.microActions.append(scout.move(enemyStartLoc.position, queue=True))
                            self.microActions.append(scout.move(self.state.mineral_field.random.position, queue=True))
                        if self.startpoint == 'NE':
                            self.microActions.append(scout.move(self.mapcorner_nw, queue=True))
                            self.microActions.append(scout.move(self.mapcorner_sw, queue=True))
                            self.microActions.append(scout.move(enemyStartLoc.position, queue=True))
                            self.microActions.append(scout.move(self.state.mineral_field.random.position, queue=True))

    async def dtattack(self):
        """Dark Templars search and destroy Workers"""
        dts = self.units(UnitTypeId.DARKTEMPLAR).ready
        if not dts:
            return

        for dt in dts:
            antidt = self.known_enemy_units.filter(lambda w: w.type_id in [UnitTypeId.OBSERVER] or w.type_id in [UnitTypeId.PHOTONCANNON] or w.type_id in [UnitTypeId.RAVEN] or w.type_id in [UnitTypeId.MISSILETURRET] or w.type_id in [UnitTypeId.OVERSEER] or w.type_id in [UnitTypeId.SPORECRAWLER]).closer_than(30, dt)
            if antidt:
                for adt in antidt:
                    distance2antidt = dt.position.distance_to(adt.position)
                    if distance2antidt < 12:
                        moveposition = dt.position.towards(adt.position, -1)
                        self.microActions.append(dt.move(moveposition))
                        break

            enemies = self.known_enemy_units.not_structure.filter(lambda w: not w.is_flying).closer_than(30, dt)
            if enemies:
                for enemy in enemies:
                    if (enemy.name == 'Probe') or (enemy.name == 'SCV') or (enemy.name == 'Drone'):
                        self.microActions.append(dt.attack(enemy.position))
                        break
            else:
                structures = self.known_enemy_structures
                if not structures:
                    await self.scoutroutine(dt, False)
                    break

                self.microActions.append(dt.move(structures.random.position, queue=True))
                break

    async def workerroutine(self):
        """Creates and assigns workers"""
        if self.units(UnitTypeId.NEXUS).ready.exists:
            nx = self.units(UnitTypeId.NEXUS).ready.first

        # Distribute idle workers
        randomnexus = self.units(UnitTypeId.NEXUS).ready
        if randomnexus:
            randomnexuspos = randomnexus.random.position
            for lazy in self.workers.idle:
                if randomnexuspos:
                    self.microActions.append(lazy.move(randomnexuspos, queue=True))
            for lazyathq in self.workers.idle:
                if nx:
                    mineral = self.state.mineral_field.closest_to(nx)
                    self.microActions.append(lazyathq.gather(mineral, queue=True))
                else:
                    break

        # Assign workers to minerals
        for hq in self.units(UnitTypeId.NEXUS).ready:
            if hq.assigned_harvesters > hq.ideal_harvesters:
                toomuch = hq.assigned_harvesters - hq.ideal_harvesters
                harvesters = self.units(UnitTypeId.PROBE).ready
                harvesters_pool = []

                harvesters_pool.extend(harvesters.random_group_of(toomuch))
                if harvesters_pool:
                    for harvester in harvesters_pool:
                        for checkhq in self.units(UnitTypeId.NEXUS).ready:
                            if checkhq.assigned_harvesters < checkhq.ideal_harvesters:
                                mineral = self.state.mineral_field.closest_to(checkhq)
                                self.microActions.append(harvester.gather(mineral))

        # Assign workers to assimilators
        if self.units(UnitTypeId.ASSIMILATOR).ready.exists:
            for gasstation in self.units(UnitTypeId.ASSIMILATOR):
                workers = self.units(UnitTypeId.PROBE)
                assigned = gasstation.assigned_harvesters
                ideal = gasstation.ideal_harvesters
                needed = ideal - assigned

                worker_pool = []
                for x in range(0, needed):
                    worker_pool.extend(workers.random_group_of(min(needed, len(workers))))
                    if worker_pool:
                        w = worker_pool.pop()
                        if len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_RETURN]:
                            self.microActions.append(w.move(gasstation))
                            self.microActions.append(w.return_resource(queue=True))
                        else:
                            self.microActions.append(w.gather(gasstation))

        # Build Workers
        self.neededworkers = 0
        workerscount = self.units(UnitTypeId.PROBE).amount
        assimilators = self.units(UnitTypeId.ASSIMILATOR)
        for a in assimilators:
            self.neededworkers = self.neededworkers + a.ideal_harvesters
            self.maxworkers = self.maxworkers + 3

        for h in self.units(UnitTypeId.NEXUS).ready:
            self.neededworkers = self.neededworkers + h.ideal_harvesters
            self.maxworkers = self.maxworkers + 16

        if (workerscount / 1.1) >= self.neededworkers:
            self.neednexus = True
        else:
            self.neednexus = False

        if (workerscount >= self.maxworkers) or (workerscount >= self.neededworkers):
            return

        for hq in self.units(UnitTypeId.NEXUS).ready:
            if self.already_pending(UnitTypeId.PROBE):
                return

            if hq.noqueue and self.can_afford(UnitTypeId.PROBE):
                self.microActions.append(hq.train(UnitTypeId.PROBE))
                return

    async def estimate(self):
        """Estimates the army value"""
        if int(self.time) & 1 == 0:
            self.my_armypower = self.units.not_structure.ready.amount - self.workers.ready.amount

        if int(self.time) & 25 == 0:
            self.my_armypower_older = self.my_armypower

        self.enemy_armypower = 0
        enemies = self.known_enemy_units.not_structure.filter(lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [UnitTypeId.SCV] and w.type_id not in [UnitTypeId.DRONE])
        for e in enemies:
                self.enemy_armypower = self.enemy_armypower + 1

        if int(self.time) & 25 == 0:
            self.enemy_armypower_older = self.enemy_armypower

        if self.enemy_armypower <= 0:
            self.enemy_armypower = 0

    async def check_retreat(self):
        if self.my_armypower < self.enemy_armypower:
            self.my_status = 'Retreat'
            fighters = self.units.not_structure.ready.filter(
                lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [
                    UnitTypeId.OBSERVER] and w.type_id not in [UnitTypeId.WARPPRISM] and w.type_id not in [
                              UnitTypeId.WARPPRISMPHASING] and w.type_id not in [
                              UnitTypeId.DARKTEMPLAR] and w.type_id not in [UnitTypeId.OBSERVERSIEGEMODE])

            for fighter in fighters:
                distance2ramp = fighter.position.distance_to(self.ramp_location)
                if distance2ramp >= 5:
                    self.microActions.append(fighter.move(self.ramp_location))


    async def estimate_status(self):
        """Estimates the status"""
        nexuslist = self.units(UnitTypeId.NEXUS)
        nexuspositions = []
        for n in nexuslist:
            nexuspositions.append(n.position)

        enemies = self.known_enemy_units.not_structure.filter(lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [UnitTypeId.SCV] and w.type_id not in [UnitTypeId.DRONE])
        enemiesattacking = 0
        for e in enemies:
            for n in nexuspositions:
                distance2nexus = e.position.distance_to(n)
                if distance2nexus <= 40:
                    enemiesattacking = enemiesattacking + 1

        if enemiesattacking >= 5:
            self.enemy_status = 'Attack'
        else:
            self.enemy_status = 'Unknown'

        # Provide output for my own Status
        if self.fightstarted:
            self.my_status = 'Attack'
        elif self.startcounter:
            self.my_status = 'Counter-Attack'
        else:
            self.my_status = 'Gather Units'

        if int(self.time) & 25 == 0:
            self.enemy_status_older = self.enemy_status
            self.my_status_older = self.my_status

    async def counterattack(self):
        if self.time >= 300:
            if (self.enemy_status_older == 'Attack' and self.enemy_status == 'Unknown') and (self.my_status != 'Attack' and self.my_armypower >= 8):
                self.startcounter = True
            elif self.my_armypower <= self.retreat_count:
                self.startcounter = False

    async def cheesedetection_voidrays(self):
        """Detects Void Rays"""
        if self.cheese_voidrays:
            return

        enemies = self.known_enemy_units
        if enemies:
            for enemy in enemies:
                if (enemy.type_id == UnitTypeId.VOIDRAY) or (enemy.type_id == UnitTypeId.STARGATE):
                    self.cheese_voidrays = True

    async def reactive_lifting_function(self):
        """Detects units that need to be lifted"""
        if self.tactic_reactive_lifting:
            if self.reactive_lifting:
                return

            enemies = self.known_enemy_units
            if enemies:
                for enemy in enemies:
                    if (enemy.type_id == UnitTypeId.SIEGETANK) or (enemy.type_id == UnitTypeId.IMMORTAL) or (enemy.type_id == UnitTypeId.LURKER) or (enemy.type_id == UnitTypeId.WIDOWMINE):
                        self.reactive_lifting = True

    async def cheesedetection_towerrush(self):
        """Detects Probe rushes - Stupidly"""
        fighterunits = self.units.not_structure.filter(lambda w: w.type_id not in [UnitTypeId.PROBE])
        if not self.units(UnitTypeId.NEXUS).ready or fighterunits.amount >= 3:
            return
        nx = self.units(UnitTypeId.NEXUS).ready.first
        enemies = self.known_enemy_units.closer_than(40, nx)

        if enemies:
            for enemy in enemies:
                if (enemy.name == 'Probe') or (enemy.name == 'Pylon') or (enemy.name == 'PhotonCannon'):
                    self.cheese_towerrush = True

                    if self.units(UnitTypeId.PHOTONCANNON).ready:
                        return

                    self.probecount = 0
                    for probe in self.workers:
                        if self.probecount <= 1:
                            self.microActions.append(probe.attack(enemy.position))
                            self.probecount = self.probecount + 1
                            break
        else:
            self.cheese_towerrush = False


    async def defendroutine(self):
        """Reacts to enemies around the Nexuses"""
        hqs = self.units(UnitTypeId.NEXUS)
        for hq in hqs:
            enemies = self.known_enemy_units.closer_than(30, hq)

            if enemies:
                self.attacked = True
                self.my_status = 'Defend'
                next_enemy = self.known_enemy_units.closest_to(hq)

                stalkers = self.units(UnitTypeId.STALKER).ready
                for stalker in stalkers:
                    self.microActions.append(stalker.attack(next_enemy.position))

                colossuses = self.units(UnitTypeId.COLOSSUS).ready
                for colossus in colossuses:
                    self.microActions.append(colossus.attack(next_enemy.position))

                immortals = self.units(UnitTypeId.IMMORTAL).ready
                for immortal in immortals:
                    self.microActions.append(immortal.attack(next_enemy.position))

                zealots = self.units(UnitTypeId.ZEALOT).ready
                for zealot in zealots:
                    self.microActions.append(zealot.attack(next_enemy.position))

                adepts = self.units(UnitTypeId.ADEPT).ready
                for adept in adepts:
                    self.microActions.append(adept.attack(next_enemy.position))
            else:
                self.attacked = False


    async def commanderroutine(self):
        """Defines a Commander"""
        fighters = self.units.not_structure.ready.filter(
            lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [
                UnitTypeId.OBSERVER] and w.type_id not in [UnitTypeId.WARPPRISM] and w.type_id not in [
                            UnitTypeId.WARPPRISMPHASING] and w.type_id not in [UnitTypeId.DARKTEMPLAR] and w.type_id not in [UnitTypeId.SENTRY] and w.type_id not in [UnitTypeId.OBSERVERSIEGEMODE])

        if (fighters.amount > self.attack_count) or self.startcounter:
            self.fightstarted = True
        else:
            self.fightstarted = False

        if fighters:
            if not self.commander:
                    possiblecommanders = sorted(fighters, key=lambda i: ((i.health_max) + (i.shield_max)), reverse=False)
                    self.commander = possiblecommanders[0].tag
            else:
                for fighter in fighters:
                    if fighter.tag == self.commander:
                        # DEBUG
                        #await self._client.debug_text("C", fighter.position)
                        self.commanderunit = fighter
                        self.commanderposition = fighter.position
                        return
                self.commanderunit = None
                self.commander = False
                self.commandertarget = None
                return


    async def fightroutine(self):
        """The main Attack and Movement Routine"""
        if not self.commander or not self.commanderunit or not self.commanderposition:
            return

        fighters = self.units.not_structure.ready.filter(
            lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [
                UnitTypeId.OBSERVER] and w.type_id not in [UnitTypeId.WARPPRISM] and w.type_id not in [
                            UnitTypeId.WARPPRISMPHASING] and w.type_id not in [UnitTypeId.DARKTEMPLAR])
        if fighters:
            for fighter in fighters:

                # Commander Action
                if fighter.tag == self.commander:

                    if not self.fightstarted:
                        d = random.randint(0, 4)
                        rampdistance = fighter.position.distance_to(self.main_base_ramp.top_center.position)
                        #checkpointposition = self._game_info.player_start_location.towards(self.game_info.map_center.position, 35)
                        checkpointposition = self.main_base_ramp.top_center.position.towards(self._game_info.player_start_location, d)
                        checkpointdistance = fighter.position.distance_to(checkpointposition)
                        # TODO: Add another waypoint
                        #nextlocation = await self.get_next_expansion()
                        #if not nextlocation:
                        #    nextlocation = checkpointposition


                        #checkpoint2position = nextlocation.position.towards(self._game_info.player_start_location, 5)
                        #checkpoint2distance = fighter.position.distance_to(checkpoint2position)

                        if self.move2ramp:
                            if rampdistance < 2:
                                self.move2ramp = False
                                break
                            else:
                                self.microActions.append(fighter.move(self.main_base_ramp.top_center.position.towards(self._game_info.player_start_location, d)))
                        elif not self.move2ramp:
                            if checkpointdistance < 2:
                                self.move2ramp = True
                                break
                            else:
                                if not self.in_pathing_grid(checkpointposition):
                                    return
                                self.microActions.append(fighter.move(checkpointposition))

                    elif self.fightstarted:
                        # Target
                        if self.known_enemy_structures:
                            target = random.choice(self.known_enemy_structures).position
                        elif self.known_enemy_units:
                            target = random.choice(self.known_enemy_units).position
                        else:
                            target = None

                        squad = self.units.not_structure.ready.filter(
                            lambda w: w.type_id not in [UnitTypeId.PROBE] and w.type_id not in [
                                UnitTypeId.OBSERVER] and w.type_id not in [UnitTypeId.WARPPRISM] and w.type_id not in [
                                          UnitTypeId.WARPPRISMPHASING] and w.type_id not in [UnitTypeId.DARKTEMPLAR]).closer_than(10, fighter)

                        if squad.amount < 6:
                            self.microActions.append(fighter.move(fighter.position))
                            break

                        if target:
                            self.microActions.append(fighter.attack(target.position))
                            self.commanderposition = target.position
                            self.commandertarget = target.position
                            break
                        else:
                            self.commandertarget = None
                            ramp = self.main_base_ramp.top_center
                            hq = self.units(UnitTypeId.NEXUS).first
                            p = ramp.position.towards(hq.position, 1)
                            distance = fighter.position.distance_to(p)
                            if distance < 2:
                                break
                            self.microActions.append(fighter.attack(p))
                            self.commanderposition = p
                            break

                # Fighter Action
                elif fighter.tag != self.commander:
                    await self.moveroutine(fighter)


    async def moveroutine(self, fighterunit):
        """Orders the given unit to move next to the Commander"""
        if self.commanderunit:
            distance = fighterunit.position.distance_to(self.commanderposition)
            if distance < 3:
                return
            target = self.commandertarget
            if not target:
                pos = self.commanderposition.towards(fighterunit.position, random.randrange(1, 3))
                if not self.in_pathing_grid(pos):
                    return
            else:
                pos = self.commanderposition.towards(target, random.randrange(3, 10))
                if not self.in_pathing_grid(pos):
                    return

            if self.fightstarted:
                self.microActions.append(fighterunit.attack(pos))
            else:
                self.microActions.append(fighterunit.attack(pos))
        else:
            return

    async def microroutine_zealot(self):
        zealots = self.units(UnitTypeId.ZEALOT).ready
        if zealots:
            for zealot in zealots:
                enemies = self.known_enemy_units.not_flying.closer_than(30, zealot)
                for e in enemies:
                    if e.is_flying:
                        enemies.remove(e)
                if enemies:
                    next_enemy = enemies.closest_to(zealot)
                    if not next_enemy:
                        break
                    distance2enemy = zealot.position.distance_to(next_enemy.position)
                    if distance2enemy < 20:
                        self.microActions.append(zealot.attack(next_enemy))

    async def microroutine_adept(self):
        adepts = self.units(UnitTypeId.ADEPT).ready

        if adepts:
            for adept in adepts:
                enemies = self.known_enemy_units.closer_than(30, adept)

                if enemies:
                    next_enemy = enemies.closest_to(adept)
                    if not next_enemy:
                        break

                    distance2enemy = adept.position.distance_to(next_enemy.position)
                    if distance2enemy < 20:
                        self.microActions.append(adept.attack(next_enemy))

                    nx = self.units(UnitTypeId.NEXUS).ready.closest_to(adept)
                    if not nx:
                        break

                    nxdistance = adept.position.distance_to(nx.position)
                    if nxdistance < 10:
                        break

                    if distance2enemy < 2:
                        break

                    if distance2enemy < 4:
                        if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                            moveposition = adept.position.towards(next_enemy.position, -1)
                            if not moveposition or not self.in_pathing_grid(moveposition):
                                break
                            self.microActions.append(adept.move(moveposition))

    async def microroutine_sentry(self):
        sentries = self.units(UnitTypeId.SENTRY).ready

        if sentries:
            for sentry in sentries:
                enemies = self.known_enemy_units.closer_than(30, sentry)

                if enemies:
                    next_enemy = enemies.closest_to(sentry)
                    if not next_enemy:
                        break
                    if (next_enemy.name == 'Probe') or (next_enemy.name == 'SCV') or (next_enemy.name == 'Drone'):
                        break

                    distance2enemy = sentry.position.distance_to(next_enemy.position)

                    ramp = self.main_base_ramp.top_center.position
                    blockpos = ramp.position.towards(self.waitlocation, 1)
                    enemydistance2ramp = next_enemy.position.distance_to(ramp)
                    sentrydistance2ramp = sentry.position.distance_to(ramp)

                    # Block Ramp
                    if (next_enemy.type_id == UnitTypeId.ZEALOT or next_enemy.type_id == UnitTypeId.MARINE or next_enemy.type_id == UnitTypeId.ZERGLING) and enemydistance2ramp <= 10 and sentry.energy >= 50:
                        self.microActions.append(sentry(AbilityId.FORCEFIELD_FORCEFIELD, blockpos))
                        break

                    # Shield
                    if not sentry.has_buff(BuffId.GUARDIANSHIELD) and (sentry.energy >= 75) and (distance2enemy < 8):
                        if (not next_enemy.name == 'Probe') or (not next_enemy.name == 'SCV') or (
                        not next_enemy.name == 'Drone'):
                            self.microActions.append(sentry(AbilityId.GUARDIANSHIELD_GUARDIANSHIELD))
                            break
                    else:
                        nx = self.units(UnitTypeId.NEXUS).ready.closest_to(sentry)
                        if not nx:
                            break

                        nxdistance = sentry.position.distance_to(nx.position)
                        if (nxdistance < 10) or (distance2enemy < 2):
                            break

                        if distance2enemy < 6:
                            if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                                moveposition = sentry.position.towards(next_enemy.position, -1)
                                if not moveposition or not self.in_pathing_grid(moveposition):
                                    break
                                self.microActions.append(sentry.move(moveposition))

    async def microroutine_stalker(self):
        stalkers = self.units(UnitTypeId.STALKER).ready

        if stalkers:
            for stalker in stalkers:
                enemies = self.known_enemy_units.closer_than(30, stalker)

                if enemies:
                    next_enemy = enemies.closest_to(stalker)
                    if not next_enemy:
                        break

                    distance2enemy = stalker.position.distance_to(next_enemy.position)
                    if distance2enemy < 20:
                        self.microActions.append(stalker.attack(next_enemy))

                    nx = self.units(UnitTypeId.NEXUS).ready.closest_to(stalker)
                    if not nx:
                        break

                    nxdistance = stalker.position.distance_to(nx.position)
                    if nxdistance < 10:
                        break

                    if distance2enemy < 6:
                        if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                            abilities = await self.get_available_abilities(stalker)
                            if (AbilityId.EFFECT_BLINK_STALKER in abilities) and (stalker.health_percentage < 25):
                                if not next_enemy.position:
                                    break
                                blinkposition = stalker.position.towards(next_enemy.position, -8)
                                if not blinkposition:
                                    break
                                if stalker.position != next_enemy.position:
                                    if self.in_pathing_grid(blinkposition) and self.is_visible(blinkposition) and not ((self.outofbounds(blinkposition)) or (blinkposition == stalker.position)):
                                        self.microActions.append(stalker(AbilityId.EFFECT_BLINK_STALKER, blinkposition))
                            elif distance2enemy < 2:
                                break
                            else:
                                moveposition = stalker.position.towards(next_enemy.position, -1)
                                if not moveposition or not self.in_pathing_grid(moveposition):
                                    break
                                self.microActions.append(stalker.move(moveposition))

    async def microroutine_immortal(self):
        immortals = self.units(UnitTypeId.IMMORTAL).ready

        if immortals:
            for immortal in immortals:
                enemies = self.known_enemy_units.closer_than(30, immortal)

                for e in enemies:
                    if e.is_flying:
                        enemies.remove(e)

                if enemies:
                    next_enemy = enemies.closest_to(immortal)
                    if not next_enemy:
                        break

                    distance2enemy = immortal.position.distance_to(next_enemy.position)
                    if distance2enemy < 20:
                        self.microActions.append(immortal.attack(next_enemy))

                    nx = self.units(UnitTypeId.NEXUS).ready.closest_to(immortal)
                    if not nx:
                        break

                    nxdistance = immortal.position.distance_to(nx.position)
                    if nxdistance < 10:
                        break

                    if distance2enemy < 2:
                        break

                    if distance2enemy < 5:
                        if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                            moveposition = immortal.position.towards(next_enemy.position, -1)
                            if not moveposition or not self.in_pathing_grid(moveposition):
                                break
                            self.microActions.append(immortal.move(moveposition))


    async def microroutine_colossus(self):
        colossuses = self.units(UnitTypeId.COLOSSUS).ready

        if colossuses:
            for colossus in colossuses:
                enemies = self.known_enemy_units.closer_than(30, colossus)
                for e in enemies:
                    if e.is_flying:
                        enemies.remove(e)

                if enemies:
                    next_enemy = enemies.closest_to(colossus)
                    if not next_enemy:
                        break

                    distance2enemy = colossus.position.distance_to(next_enemy.position)
                    if distance2enemy < 20:
                        self.microActions.append(colossus.attack(next_enemy))

                    nx = self.units(UnitTypeId.NEXUS).ready.closest_to(colossus)
                    if not nx:
                        break

                    nxdistance = colossus.position.distance_to(nx.position)
                    if nxdistance < 5:
                        break

                    if distance2enemy < 1:
                        break

                    if self.exlance_started:
                        movedistance = 9
                    else:
                        movedistance = 7

                    if distance2enemy < movedistance:
                        if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                            moveposition = colossus.position.towards(next_enemy.position, -1)
                            if not moveposition or not self.in_pathing_grid(moveposition):
                                break
                            self.microActions.append(colossus.move(moveposition))

    async def microroutine_observer(self):
        observers = self.units(UnitTypeId.OBSERVER).ready
        observersiege = self.units(UnitTypeId.OBSERVERSIEGEMODE).ready
        allobservers = observers | observersiege

        if allobservers:
            for observer in allobservers:
                enemies = self.known_enemy_units.closer_than(15, observer)
                if enemies:
                    if enemies.amount > 10:
                        abilities = await self.get_available_abilities(observer)
                        if AbilityId.MORPH_SURVEILLANCEMODE in abilities:
                            if self.can_afford(AbilityId.MORPH_SURVEILLANCEMODE):
                                self.microActions.append(observer(AbilityId.MORPH_SURVEILLANCEMODE))
                                break
                    else:
                        abilities = await self.get_available_abilities(observer)
                        if AbilityId.MORPH_OBSERVERMODE in abilities:
                            if self.can_afford(AbilityId.MORPH_OBSERVERMODE):
                                self.microActions.append(observer(AbilityId.MORPH_OBSERVERMODE))
                                break
                else:
                    abilities = await self.get_available_abilities(observer)
                    if AbilityId.MORPH_OBSERVERMODE in abilities:
                        if self.can_afford(AbilityId.MORPH_OBSERVERMODE):
                            self.microActions.append(observer(AbilityId.MORPH_OBSERVERMODE))
                            break
                if observer.type_id == UnitTypeId.OBSERVER:
                    await self.scoutroutine(observer, False)

    async def microroutine_phoenix(self):
        phoenixs = self.units(UnitTypeId.PHOENIX).ready

        if phoenixs:
            for phoenix in phoenixs:
                enemystructures = self.known_enemy_units.structure
                if enemystructures:
                    for es in enemystructures:
                        if es.type_id == UnitTypeId.PHOTONCANNON:
                            distance2antiair = phoenix.position.distance_to(es.position)
                            if distance2antiair < 8:
                                moveposition = phoenix.position.towards(es.position, -1)
                                self.microActions.append(phoenix.move(moveposition))
                                break

                # Ground
                enemies = self.known_enemy_units.not_structure.filter(lambda w: not w.is_flying).closer_than(30, phoenix)
                if enemies:
                    next_enemy = enemies.closest_to(phoenix)
                    if not next_enemy:
                        break

                    distance2enemy = phoenix.position.distance_to(next_enemy.position)
                    if distance2enemy < 5 and ((next_enemy.type_id == UnitTypeId.SIEGETANK) or (next_enemy.type_id == UnitTypeId.IMMORTAL) or (next_enemy.type_id == UnitTypeId.LURKER) or (next_enemy.type_id == UnitTypeId.WIDOWMINE)):
                        abilities = await self.get_available_abilities(phoenix)
                        if AbilityId.GRAVITONBEAM_GRAVITONBEAM in abilities:
                            self.microActions.append(phoenix(AbilityId.GRAVITONBEAM_GRAVITONBEAM, next_enemy))
                            break
                    if distance2enemy < 4:
                        if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                            moveposition = phoenix.position.towards(next_enemy.position, -1)
                            if not moveposition:
                                break
                            self.microActions.append(phoenix.move(moveposition))
                    elif distance2enemy > 10:
                        if (next_enemy.name != 'Probe') or (next_enemy.name != 'SCV') or (next_enemy.name != 'Drone'):
                            moveposition = phoenix.position.towards(next_enemy.position, 1)
                            self.microActions.append(phoenix.move(moveposition))
                # Air
                flyingenemies = self.known_enemy_units.not_structure.filter(lambda w: w.is_flying).closer_than(10,
                                                                                                             phoenix)
                if flyingenemies:
                    next_enemy = flyingenemies.closest_to(phoenix)
                    if not next_enemy:
                        break
                    self.microActions.append(phoenix.attack(next_enemy))

    async def microroutine_warpprism(self):
        warpprisms = self.units(UnitTypeId.WARPPRISM).ready

        if not self.warpdrop:
            enemystructures = self.known_enemy_units.structure
            for enemystructure in enemystructures:
                if (enemystructure.name == 'Refinery') or (enemystructure.name == 'Extractor') or (enemystructure.name == 'Hatchery') or (enemystructure.name == 'Assimilator'):
                    self.warpdrop = enemystructure.position

        if warpprisms:
            for warpprism in warpprisms:
                if self.warpdrop:
                    distance2drop = warpprism.position.distance_to(self.warpdrop)
                    if distance2drop <= 10:
                        abilities = await self.get_available_abilities(warpprism)
                        if AbilityId.MORPH_WARPPRISMPHASINGMODE in abilities:
                            if self.can_afford(AbilityId.MORPH_WARPPRISMPHASINGMODE):
                                self.microActions.append(warpprism(AbilityId.MORPH_WARPPRISMPHASINGMODE))
                                break
                    else:
                        if warpprism.noqueue and self.startpoint:
                            if self.startpoint == 'SW':
                                self.microActions.append(warpprism.move(self.mapcorner_se, queue=True))
                                self.microActions.append(warpprism.move(self.mapcorner_ne, queue=True))
                                self.microActions.append(warpprism.move(self.warpdrop, queue=True))
                            if self.startpoint == 'SE':
                                self.microActions.append(warpprism.move(self.mapcorner_sw, queue=True))
                                self.microActions.append(warpprism.move(self.mapcorner_nw, queue=True))
                                self.microActions.append(warpprism.move(self.warpdrop, queue=True))
                            if self.startpoint == 'NW':
                                self.microActions.append(warpprism.move(self.mapcorner_ne, queue=True))
                                self.microActions.append(warpprism.move(self.mapcorner_se, queue=True))
                                self.microActions.append(warpprism.move(self.warpdrop, queue=True))
                            if self.startpoint == 'NE':
                                self.microActions.append(warpprism.move(self.mapcorner_nw, queue=True))
                                self.microActions.append(warpprism.move(self.mapcorner_sw, queue=True))
                                self.microActions.append(warpprism.move(self.warpdrop, queue=True))



    async def trainroutine(self):
        """Unit creation"""
        if self.supply_used == self.supply_cap:
            return

        if self.neednexus and not self.already_pending(UnitTypeId.NEXUS):
            return

        unitlist = get_unitlist(str(self.enemyrace))
        for i in unitlist['unit']:
            unitname = (unitlist['unit'][i]['name'])
            buildstructure = (unitlist['unit'][i]['structure'])
            buildrequirement = (unitlist['unit'][i]['requirement'])
            unitcount = (unitlist['unit'][i]['count']) * self.units(UnitTypeId.NEXUS).amount

            # Dont spawn DT when enemy has cannons
            if unitname == 'DARKTEMPLAR' and self.enemycannons:
                continue

            # Spawn observer only when needed
            if not (self.known_enemy_structures and self.warpdrop) and not (self.units(UnitTypeId.OBSERVER).exists or self.units(UnitTypeId.OBSERVERSIEGEMODE).exists):
                if not self.already_pending(UnitTypeId.OBSERVER):
                        if self.can_afford(UnitTypeId.OBSERVER):
                            for structure in self.units(UnitTypeId.ROBOTICSFACILITY).ready.idle:
                                self.microActions.append(structure.train(UnitTypeId.OBSERVER))
                                if self.debugmsg:
                                    print("Training " + str(UnitTypeId.OBSERVER))
                                break

            # Spawn a WarpPrism
            if self.tactic_warpprism:
                if self.units(UnitTypeId.ROBOTICSFACILITY).ready.exists and self.warpdrop and not self.enemycannons and self.minerals >= 500:
                    if not self.already_pending(UnitTypeId.WARPPRISM) and not (self.units(UnitTypeId.WARPPRISM).exists or self.units(UnitTypeId.WARPPRISMPHASING).exists):
                        if self.can_afford(UnitTypeId.WARPPRISM):
                            for structure in self.units(UnitTypeId.ROBOTICSFACILITY).ready.idle:
                                self.microActions.append(structure.train(UnitTypeId.WARPPRISM))
                                if self.debugmsg:
                                    print("Training " + str(UnitTypeId.WARPPRISM))
                                break

                # Send Zealots when we have a Prism!
                if self.units(UnitTypeId.WARPPRISMPHASING).exists:
                    unitname = 'ZEALOT'

            # Spawn Phoenix as counter
            if (self.cheese_voidrays or self.reactive_lifting) and self.units(UnitTypeId.STALKER).amount >= 4:
                if self.units(UnitTypeId.PHOENIX).amount <= 3:
                    if not self.already_pending(UnitTypeId.PHOENIX):
                        if self.units(UnitTypeId.STARGATE).ready.exists:
                            if self.can_afford(UnitTypeId.PHOENIX):
                                for structure in self.units(UnitTypeId.STARGATE).ready.idle:
                                    self.microActions.append(structure.train(UnitTypeId.PHOENIX))
                                    if self.debugmsg:
                                        print("Training " + str(UnitTypeId.PHOENIX))
                                    break

            # Make sure we dont get more than 6 zeals, whatever the nexuscount is, and we prefer stalkers over zeals
            zealcount = self.units(UnitTypeId.ZEALOT).amount
            adeptscount = self.units(UnitTypeId.ADEPT).amount
            sentriescount = self.units(UnitTypeId.SENTRY).amount
            stalkerscount = self.units(UnitTypeId.STALKER).amount
            immortalscount = self.units(UnitTypeId.IMMORTAL).amount
            colossuscount = self.units(UnitTypeId.COLOSSUS).amount
            templercount = self.units(UnitTypeId.DARKTEMPLAR).amount
            prismcount = self.units(UnitTypeId.WARPPRISM).amount

            if not self.units(UnitTypeId.WARPPRISMPHASING).exists:
                if unitname == 'ZEALOT' and ((self.units(UnitTypeId.WARPGATE).exists and stalkerscount <= 6) or (zealcount >= 6) or self.time >= 600):
                    continue

            if unitname == 'ADEPT' and ((self.units(UnitTypeId.WARPGATE).exists and stalkerscount <= 6) or (adeptscount >= 6)):
                continue

            if unitname == 'STALKER' and (stalkerscount >= 15 and self.minerals <= 450):
                continue

            if unitname == 'IMMORTAL' and immortalscount >= 5:
                continue

            if unitname == 'COLOSSUS' and colossuscount >= 3:
                continue

            if unitname == 'SENTRY' and sentriescount >= 3:
                continue

            if unitname == 'WARPPRISM' and prismcount >= 1:
                continue

            if unitname == 'DARKTEMPLAR' and (self.time >= 600 or templercount >= 5 or not self.tactic_darktemplar):
                continue

            if buildrequirement:
                if not self.units(UnitTypeId[buildrequirement]).ready.exists:
                    continue

            if not self.units(UnitTypeId[buildstructure]).ready.exists:
                continue

            if self.already_pending(UnitTypeId[unitname]):
                continue

            if not self.can_afford(UnitTypeId[unitname]):
                continue

            if self.units(UnitTypeId[unitname]).amount >= unitcount:
                continue

            # the unit train routine
            for structure in self.units(UnitTypeId[buildstructure]).ready.idle:
                if buildstructure == 'WARPGATE':
                    abilities = await self.get_available_abilities(structure)
                    if AbilityId.WARPGATETRAIN_STALKER in abilities:
                        if self.units(UnitTypeId.WARPPRISMPHASING).ready.exists:
                            proxy = self.units(UnitTypeId.WARPPRISMPHASING).ready.first
                        elif self.fightstarted or unitname == 'DARKTEMPLAR':
                            proxy = self.units(UnitTypeId.PYLON).closest_to(
                                self.enemy_start_locations[0])
                        elif not self.fightstarted:
                            ramp = self.main_base_ramp.top_center.position
                            proxy = self.units(UnitTypeId.PYLON).closest_to(ramp)

                        pos = proxy.position.to2.random_on_distance(4)
                        if pos:
                            possible_positions = {pos + Point2((x, y)) for x in range(-5, 6) for y in range(-5, 6)}
                            better_positions = pos.sort_by_distance(possible_positions)
                            for placement in better_positions:
                                if placement:
                                    if self.in_placement_grid(placement) and self.is_visible(placement):
                                        self.microActions.append(structure.warp_in(UnitTypeId[unitname], placement))
                                        if self.debugmsg:
                                            print("Warping " + str(UnitTypeId[unitname]))
                                            return

                    elif AbilityId.GATEWAYTRAIN_STALKER in abilities:
                        self.microActions.append(structure.train(UnitTypeId[unitname]))
                        if self.debugmsg:
                            print("Training " + str(UnitTypeId[unitname]))
                            return

                else:
                    self.microActions.append(structure.train(UnitTypeId[unitname]))
                    if self.debugmsg:
                        print("Training " + str(UnitTypeId[unitname]))
                        return

    async def buffroutine(self):
        """Research Buffs and perform Boosts"""
        if self.neednexus:
            return

        for hq in self.units(UnitTypeId.NEXUS).ready:

            # Boost Cyber Core
            if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists and not self.coreboosted:
                ccore = self.units(UnitTypeId.CYBERNETICSCORE).ready.first
                if not ccore.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.microActions.append(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, ccore))
                        self.coreboosted = True
                        if self.debugmsg:
                            print("Boosting UnitTypeId.CYBERNETICSCORE")
                        return

            # Boost Gateway
            if self.units(UnitTypeId.GATEWAY).ready.exists and self.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                gw = self.units(UnitTypeId.GATEWAY).ready.first
                if not gw.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.microActions.append(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, gw))
                        if self.debugmsg:
                            print("Boosting UnitTypeId.GATEWAY")
                        return

            # Boost Warpgate
            if self.units(UnitTypeId.WARPGATE).ready.exists and self.units(UnitTypeId.CYBERNETICSCORE).ready.exists:
                warps = self.units(UnitTypeId.WARPGATE).ready.first
                if not warps.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                    abilities = await self.get_available_abilities(hq)
                    if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in abilities:
                        self.microActions.append(hq(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, warps))
                        if self.debugmsg:
                            print("Boosting UnitTypeId.WARPGATE")
                        return

            # WARPGATE
            if self.units(UnitTypeId.CYBERNETICSCORE).ready.exists and self.can_afford(
                    AbilityId.RESEARCH_WARPGATE) and not self.warpgate_started:
                ccore = self.units(UnitTypeId.CYBERNETICSCORE).ready.first
                self.microActions.append(ccore(AbilityId.RESEARCH_WARPGATE))
                self.warpgate_started = True
                if self.debugmsg:
                    print("Researching AbilityId.RESEARCH_WARPGATE")
                return

            for gateway in self.units(UnitTypeId.GATEWAY).ready:
                abilities = await self.get_available_abilities(gateway)
                if AbilityId.MORPH_WARPGATE in abilities and self.can_afford(AbilityId.MORPH_WARPGATE):
                    self.microActions.append(gateway(AbilityId.MORPH_WARPGATE))
                    if self.debugmsg:
                        print("Morphing UnitTypeId.GATEWAY")
                    return

            # Blink
            if self.units(UnitTypeId.TWILIGHTCOUNCIL).ready.exists and self.can_afford(
                    AbilityId.RESEARCH_BLINK) and not self.blink_started:
                tc = self.units(UnitTypeId.TWILIGHTCOUNCIL).ready.first
                self.microActions.append(tc(AbilityId.RESEARCH_BLINK))
                self.blink_started = True
                if self.debugmsg:
                    print("Researching AbilityId.RESEARCH_BLINK")
                return

            if self.units(UnitTypeId.STALKER).ready and not self.blink_done:
                stalker = self.units(UnitTypeId.STALKER).ready.first
                abilities = await self.get_available_abilities(stalker)
                if AbilityId.EFFECT_BLINK_STALKER in abilities:
                    self.blink_done = True

            # Extended Thermal Lance
            if self.units(UnitTypeId.COLOSSUS).amount >= 1:
                if self.units(UnitTypeId.ROBOTICSBAY).ready.exists and self.can_afford(
                        AbilityId.RESEARCH_EXTENDEDTHERMALLANCE) and not self.exlance_started:
                    tc = self.units(UnitTypeId.ROBOTICSBAY).ready.first
                    self.microActions.append(tc(AbilityId.RESEARCH_EXTENDEDTHERMALLANCE))
                    self.exlance_started = True
                    if self.debugmsg:
                        print("Researching AbilityId.RESEARCH_EXTENDEDTHERMALLANCE")
                    return

            # Get upgrades a little bit later
            if self.time >= 600:
                if self.enemyrace == Race.Zerg:
                    # Ground Attack Boost LvL 1
                    if self.units(UnitTypeId.FORGE).ready.exists and self.can_afford(
                            AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL1) and not self.attacklvl1_started:
                        tc = self.units(UnitTypeId.FORGE).ready.first
                        self.microActions.append(tc(AbilityId.RESEARCH_PROTOSSGROUNDWEAPONS))
                        self.attacklvl1_started = True
                        if self.debugmsg:
                            print("Researching AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL1")
                        return

                    # Ground Attack Boost LvL 2
                    if self.units(UnitTypeId.FORGE).ready.exists:
                        if self.units(UnitTypeId.TWILIGHTCOUNCIL).ready.exists and self.can_afford(
                                AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL2) and not self.attacklvl2_started:
                            tc = self.units(UnitTypeId.FORGE).ready.first
                            self.microActions.append(tc(AbilityId.RESEARCH_PROTOSSGROUNDWEAPONS))
                            self.attacklvl2_started = True
                            if self.debugmsg:
                                print("Researching AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL2")
                            return

                    # Ground Attack Boost LvL 3
                    if self.units(UnitTypeId.FORGE).ready.exists:
                        if self.units(UnitTypeId.TWILIGHTCOUNCIL).ready.exists and self.can_afford(
                                AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL3) and not self.attacklvl3_started:
                            tc = self.units(UnitTypeId.FORGE).ready.first
                            self.microActions.append(tc(AbilityId.RESEARCH_PROTOSSGROUNDWEAPONS))
                            self.attacklvl3_started = True
                            if self.debugmsg:
                                print("Researching AbilityId.FORGERESEARCH_PROTOSSGROUNDWEAPONSLEVEL3")
                            return

                if self.enemyrace == Race.Terran:
                    # Ground Armor Boost LvL 1
                    if self.units(UnitTypeId.FORGE).ready.exists and self.can_afford(
                            AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL1) and not self.armorlvl1_started:
                        tc = self.units(UnitTypeId.FORGE).ready.first
                        self.microActions.append(tc(AbilityId.RESEARCH_PROTOSSGROUNDARMOR))
                        self.armorlvl1_started = True
                        if self.debugmsg:
                            print("Researching AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL1")
                        return

                    # Ground Armor Boost LvL 2
                    if self.units(UnitTypeId.FORGE).ready.exists:
                        if self.units(UnitTypeId.TWILIGHTCOUNCIL).ready.exists and self.can_afford(
                                AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL2) and not self.armorlvl2_started:
                            tc = self.units(UnitTypeId.FORGE).ready.first
                            self.microActions.append(tc(AbilityId.RESEARCH_PROTOSSGROUNDARMOR))
                            self.armorlvl2_started = True
                            if self.debugmsg:
                                print("Researching AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL2")
                            return

                    # Ground Armor Boost LvL 3
                    if self.units(UnitTypeId.FORGE).ready.exists:
                        if self.units(UnitTypeId.TWILIGHTCOUNCIL).ready.exists and self.can_afford(
                                AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL3) and not self.armorlvl3_started:
                            tc = self.units(UnitTypeId.FORGE).ready.first
                            self.microActions.append(tc(AbilityId.RESEARCH_PROTOSSGROUNDARMOR))
                            self.armorlvl3_started = True
                            if self.debugmsg:
                                print("Researching AbilityId.FORGERESEARCH_PROTOSSGROUNDARMORLEVEL3")
                            return

    async def buildroutine(self):
        """Creates Structures"""

        # Nexus Loop
        for hq in self.units(UnitTypeId.NEXUS).ready:
            # Read nexusInfo
            for unitTag, nexusInfo in self.nexusInfo.items():
                unitImLookingFor = self.units.find_by_tag(unitTag)

            currentphase = str(nexusInfo['phase'])
            buildorder = get_buildorder(str(self.enemyrace))
            buildproject = buildorder['phase'][currentphase]['name']
            buildrequirement = buildorder['phase'][currentphase]['requirement']
            buildsupply = buildorder['phase'][currentphase]['supply']

            # PYLONS
            if self.supply_left < self.pylonthreshold:
                if not self.already_pending(UnitTypeId.PYLON) and self.can_afford(UnitTypeId.PYLON):
                    worker = self.select_build_worker(hq.position)
                    if worker:
                        pylonposition = hq.position.random_on_distance(random.randrange(1, 15))
                        nextmineral = self.state.mineral_field.closest_to(pylonposition)
                        distance2buildplace = nextmineral.position.distance_to(pylonposition)
                        if distance2buildplace > 6:
                            if not self.state.psionic_matrix.covers(pylonposition):
                                err = await self.build(UnitTypeId.PYLON, near=pylonposition)
                                if not err:
                                    if self.debugmsg:
                                        print("Building UnitTypeId.PYLON")
                                    break

            # Anti-Rush Cannons!
            if self.enemyrush and not self.enemyrush_cannons_done:
                if not self.already_pending(UnitTypeId.FORGE) and not self.units(UnitTypeId.FORGE).exists:
                    if self.can_afford(UnitTypeId.FORGE):
                        ramp = self.main_base_ramp.top_center.position
                        p = hq.position.towards(ramp.position, 1)
                        stepsize = 2
                        err = await self.build(UnitTypeId.FORGE, near=p, placement_step=stepsize)
                        if not err:
                            if self.debugmsg:
                                print("Building UnitTypeId.FORGE")
                                return
                            break
                    else:
                        return

                if self.units(UnitTypeId.FORGE).ready.exists:
                    cannons = self.units(UnitTypeId.PHOTONCANNON).amount
                    if cannons >= 4:
                        self.enemyrush_cannons_done = True
                        return

                    if self.can_afford(UnitTypeId.PHOTONCANNON):
                        ramp = self.main_base_ramp.top_center.position
                        if cannons <= 2:
                            p = ramp.position.towards(hq.position, 3)
                            stepsize = 2
                        else:
                            p = ramp.position.towards(hq.position, 4)
                            stepsize = 2
                        err = await self.build(UnitTypeId.PHOTONCANNON, near=p, placement_step=stepsize)
                        if not err:
                            if self.debugmsg:
                                print("Building UnitTypeId.PHOTONCANNON")
                                return
                            break
                    else:
                        return
                else:
                    return


            # STARGATE
            if self.cheese_voidrays or self.reactive_lifting:
                if not self.already_pending(UnitTypeId.STARGATE) and not self.units(UnitTypeId.STARGATE).exists and self.units(UnitTypeId.CYBERNETICSCORE).ready.exists and self.can_afford(UnitTypeId.STARGATE):
                    ramp = self.main_base_ramp.top_center.position
                    p = hq.position.towards(ramp.position, 3)
                    stepsize = 2
                    nextmineral = self.state.mineral_field.closest_to(p)
                    distance2mineral = nextmineral.position.distance_to(p)
                    if distance2mineral > 6:
                        err = await self.build(UnitTypeId.STARGATE, near=p, placement_step=stepsize)
                        if not err:
                            if self.debugmsg:
                                print("Building UnitTypeId.STARGATE")
                                return
                            break

            # NEXUS
            if self.neednexus or self.can_afford(UnitTypeId.NEXUS):
                nexuscount = self.units(UnitTypeId.NEXUS).amount
                if self.time >= 900:
                    timenexus = 4
                elif self.time >= 600:
                    timenexus = 3
                elif self.time >= 360:
                    timenexus = 2
                else:
                    timenexus = False

                if nexuscount <= self.maxhqcount and timenexus:
                    if nexuscount <= timenexus:
                        if not self.already_pending(UnitTypeId.NEXUS):
                            location = await self.get_next_expansion()
                            if location:
                                err = await self.build(UnitTypeId.NEXUS, location)
                                if not err:
                                    break

            # This HQ is done
            if buildproject == 'END':
                # Lets build some defense
                self.enemyrush = True
                return

            # Raise the phase if build is pending
            for buildinprogress in self.units(UnitTypeId[buildproject]):
                if buildinprogress.build_progress < 0.1 and buildproject != 'ASSIMILATOR':
                    nexusInfo['phase'] = nexusInfo['phase'] + 1
                    return

            # Assimilators
            vgs = self.state.vespene_geyser.closer_than(15, hq)
            for vg in vgs:
                worker = self.select_build_worker(vg.position)
                if worker is None:
                    break
                if not self.units(UnitTypeId.ASSIMILATOR).closer_than(1.0, vg).exists:
                    if self.can_afford(UnitTypeId.ASSIMILATOR):
                        self.microActions.append(worker.build(UnitTypeId.ASSIMILATOR, vg))

            else:
                # We need only one of them
                if buildproject == 'CYBERNETICSCORE':
                    if self.units(UnitTypeId[buildproject]).ready.exists:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                if buildproject == 'ROBOTICSBAY':
                    if self.units(UnitTypeId.ROBOTICSBAY).ready.exists:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                if buildproject == 'SHIELDBATTERY':
                    if self.units(UnitTypeId[buildproject]).ready.exists:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                if buildproject == 'PHOTONCANNON':
                    if self.units(UnitTypeId[buildproject]).ready.exists:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                if buildproject == 'TWILIGHTCOUNCIL':
                    if self.units(UnitTypeId[buildproject]).ready.exists:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                # We dont want DTs when the enemy has cannons
                if buildproject == 'DARKSHRINE':
                    if self.units(UnitTypeId[buildproject]).ready.exists or self.enemycannons or not self.tactic_darktemplar:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                if buildproject == 'FORGE':
                    if self.units(UnitTypeId[buildproject]).ready.exists:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                # Two please...
                if buildproject == 'ROBOTICSFACILITY':
                    rbcount = self.units(UnitTypeId.ROBOTICSBAY).ready.amount
                    if rbcount >= 2:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                if buildproject == 'GATEWAY':
                    gwcount = self.units(UnitTypeId.STARGATE).amount + self.units(UnitTypeId.WARPGATE).amount
                    if gwcount >= 2:
                        nexusInfo['phase'] = nexusInfo['phase'] + 1
                        return

                if buildrequirement:
                    if not self.units(UnitTypeId[buildrequirement]).ready.exists:
                        break

                if buildsupply:
                    if self.supply_cap < buildsupply:
                        break

                if self.already_pending(UnitTypeId[buildproject]):
                    break

                if self.can_afford(UnitTypeId[buildproject]):
                    ramp = self.main_base_ramp.top_center

                    if buildproject == 'SHIELDBATTERY':
                        p = ramp.position.towards(hq.position, 5)
                        stepsize = 2
                    elif buildproject == 'PHOTONCANNON':
                        p = hq.position.towards(ramp.position, 1)
                        stepsize = 2
                    else:
                        p = hq.position.towards(ramp.position, 5)
                        stepsize = 2

                    nextmineral = self.state.mineral_field.closest_to(p)
                    distance2mineral = nextmineral.position.distance_to(p)
                    if distance2mineral > 6:
                        workers = self.units(UnitTypeId.PROBE).ready
                        if not workers:
                            break

                        for worker in workers.filter(lambda w: w.is_idle or w.is_gathering):
                            err = await self.build(UnitTypeId[buildproject], near=p, unit=worker, placement_step=stepsize)
                            if not err:
                                if self.debugmsg:
                                    print("Building " + str(UnitTypeId[buildproject]))
                                    return
                                break

    async def mapbounds(self):
        raw_map = self.game_info.playable_area
        map_width = raw_map[2]
        map_height = raw_map[3]
        playerstart = self.game_info.player_start_location

        self.mapcorner_sw = Point2((0, 0))
        self.mapcorner_se = Point2((map_width, 0))
        self.mapcorner_nw = Point2((0, map_height))
        self.mapcorner_ne = Point2((map_width, map_height))
        self.map_center_n = Point2(((map_width / 2), map_height))
        self.map_center_s = Point2(((map_width / 2), 0))
        self.map_center_w = Point2((0, (map_height / 2)))
        self.map_center_e = Point2((map_width, (map_height / 2)))
        self.map_center = Point2(((map_width / 2), (map_height / 2)))

        if playerstart.distance_to(self.mapcorner_sw) < 50:
            self.startpoint = 'SW'
        if playerstart.distance_to(self.mapcorner_se) < 50:
            self.startpoint = 'SE'
        if playerstart.distance_to(self.mapcorner_nw) < 50:
            self.startpoint = 'NW'
        if playerstart.distance_to(self.mapcorner_ne) < 50:
            self.startpoint = 'NE'

    def outofbounds(self, pos):
        if not pos:
            return
        raw_map = self.game_info.playable_area
        map_width = raw_map[2]
        map_height = raw_map[3]

        x = pos[0]
        y = pos[1]

        if (x <= 0) or (x >= map_width):
            return True
        elif (y <= 0) or (y >= map_height):
            return True

    async def on_step(self, iteration):
        """Runs on every Gamestep"""
        if iteration == 0:
            self.scoutunits = self.units & []
            self._client.game_step = 8
            self.waitlocation = await self.get_next_expansion()
            await self.mapbounds()

        self.microActions = []
        self.targetunits = []

        if not self.known_enemy_structures:
            self.scouting = False

        if self.time >= 20 and self.time <= 120:
            await self.waitforit()

        if self.statusmsg:
            if int(self.time) & 1 == 0:
                clearscreen()
                print("My Army:             " + str(self.my_armypower))
                print("My Army (-30s):      " + str(self.my_armypower_older))
                print("My Status:           " + self.my_status)
                print("My Status (-30s):    " + self.my_status_older)

                print("Enemy Army:          " + str(self.enemy_armypower))
                print("Enemy Army (-30s):   " + str(self.enemy_armypower_older))
                print("Enemy Status:        " + self.enemy_status)
                print("Enemy Status (-30s): " + self.enemy_status_older)

        if iteration & 2 == 0:
            await self.estimate()
            await self.estimate_status()

        if not self.scouting:
            await self.scoutwithworker()

        if self.enemyrace == "Unknown":
            await self.racecheck()
            return

        # Kamikaze
        nexus = self.units(UnitTypeId.NEXUS)
        if not nexus.exists:
            target = self.known_enemy_structures.random_or(self.enemy_start_locations[0]).position
            for unit in self.workers | self.units.not_structure:
                self.microActions.append(unit.attack(target))
                return

        # TAGGING
        for hq in self.units(UnitTypeId.NEXUS).ready:
            if hq.tag not in self.nexusInfo:
                self.nexusInfo[hq.tag] = {'phase': 1, 'type': 'structure'}

        for u in self.units.not_structure.ready:
            if u.tag not in self.unitInfo:
                self.unitInfo[u.tag] = {'worker': 0, 'scout': 0, 'type': 'unit'}

        if self.opponent_id:
            datafile = "./data/" + self.opponent_id + ".db"
            if not os.path.isfile(datafile):
                await self.timecheck()
            else:
                if not self.reacted:
                    await self.readdata()

        await self.counterattack()
        await self.reactontime()
        await self.reactonworkerrush()
        await self.reactoncannons()
        await self.dtattack()
        await self.cheesedetection_towerrush()
        await self.cheesedetection_voidrays()
        await self.reactive_lifting_function()
        await self.buildatramp()
        await self.buildroutine()
        await self.workerroutine()
        await self.buffroutine()
        await self.trainroutine()
        await self.commanderroutine()
        await self.fightroutine()
        await self.microroutine_zealot()
        await self.microroutine_adept()
        await self.microroutine_sentry()
        await self.microroutine_stalker()
        await self.microroutine_immortal()
        await self.microroutine_colossus()
        await self.microroutine_observer()
        await self.microroutine_phoenix()
        await self.microroutine_warpprism()
        await self.check_retreat()
        await self.defendroutine()
        if self.tactic_buildproxy:
            await self.buildproxy()

        await self.do_actions(self.microActions)
